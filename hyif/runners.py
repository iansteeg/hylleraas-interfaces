import pathlib
import subprocess
import warnings
from abc import ABC
from typing import Callable, Tuple

from hyset import ComputeSettings


class Runners(ABC):
    """Base class for runners."""

    run: Callable
    run_async: Callable


class RunnerFileStdoutFile(Runners):
    """Running program with in and output file."""

    def __init__(self, program: str, env: ComputeSettings):
        if not isinstance(program, str):
            raise Exception('Input error - string input required')

        self.program = program
        self.work_dir = env.work_dir
        self.env = env.env

    def run(self, infile: str, outfile: str) -> str:
        """Run a program."""
        inpath = pathlib.Path(self.work_dir) / pathlib.Path(infile)
        if not inpath.exists():
            raise Exception(f'could not find inputfile {inpath}')

        path = pathlib.Path(self.work_dir) / pathlib.Path(outfile)
        # stdout = open(path, 'wt')
        # stderr = subprocess.PIPE

        if path.exists():
            # warnings.warn(f'outputfile {path} already exists,
            # will skip computation...')
            with open(path, 'r') as output_file:
                output = output_file.read()
            return output

        try:
            result = subprocess.run([self.program, infile],
                                    capture_output=True,
                                    text=True,
                                    cwd=self.work_dir,
                                    env=self.env,
                                    shell=False)
        except Exception:
            raise Exception('subprocess run error')
        else:
            if result.stderr:
                if 'normal termination of xtb' not in result.stderr:
                    warnings.warn(result.stderr)
            result.check_returncode()

        with open(path, 'wt') as output_file:
            output_file.write(result.stdout)

        return str(path)

    def run_async(self, infile: str, outfile: str) -> Tuple[str, int]:
        """Run a program asynchronous."""
        if not pathlib.Path(infile).exists():
            raise Exception(f'could not find inputfile {infile}')

        path = pathlib.Path(self.work_dir) / pathlib.Path(outfile)

        # stdout =  subprocess.PIPE
        stdout = open(path, 'wt')
        stderr = subprocess.PIPE

        with subprocess.Popen(
            [self.program, infile],
                text=True,
                cwd=self.work_dir,
                env=self.env,
                shell=False,
                stdout=stdout,
                stderr=stderr,
        ) as process:
            try:
                pid = process.pid
                # stdout, stderr = process.communicate()
            except Exception:
                process.kill()
            else:
                return outfile, pid
                # process.wait()
            finally:
                retcode = process.poll()

            if retcode:
                raise Exception(f'run failed with errorcode {retcode}')

            # if stderr:
            #     warnings.warn(stderr)
        stdout.close()
        return outfile, pid


class RunnerFileStdout(Runners):
    """Running program with input file and output to stdout."""

    def __init__(self, program: str, env: ComputeSettings):
        if not isinstance(program, str):
            raise Exception('Input error - string input required')

        self.program = program
        self.work_dir = env.work_dir
        self.env = env.env

    def run(self, *args) -> str:
        """Run a program."""
        # if not pathlib.Path(infile).exists():
        #     raise Exception(f'could not find inputfile {infile}')
        try:
            result = subprocess.run([self.program, *args],
                                    capture_output=True,
                                    text=True,
                                    cwd=self.work_dir,
                                    env=self.env,
                                    shell=False)
        except Exception:
            raise Exception('subprocess run error')
        else:
            result.check_returncode()

        if result.stderr:
            if 'normal termination of xtb' not in result.stderr:
                warnings.warn(result.stderr)

        return result.stdout

    def run_async(self, *args, **kwargs):
        """Run program asynchronous."""
        pass
