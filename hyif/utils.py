import collections.abc
import copy
import inspect
from pathlib import Path
from typing import Any, Generator, List, Optional, Union

# from qcelemental import PhysicalConstantsContext

# constants = PhysicalConstantsContext('CODATA2018')
# bohr2angstrom = constants.bohr2angstroms

# class DefaultLegacyRuns:

#     def run(self, object, *args, **kwargs):
#         with self.compute_settings.compute(self, object,
# *args, **kwargs) as r:
#             return self.parse(r)

#     async def arun(self, object, *args, **kwargs):
#         async with self.compute_settings.acompute(self, object, *args,
#  **kwargs) as r:
#             return self.parse(r)
# def run_sequentially(func):
#     """Decorator for performing a sequence of runs."""
#     @wraps(func)
#     def wrapper(self, arg, *args, **kwargs):
#         # normal case
#         if not isinstance(arg, list):
#             return func(self, arg, *args, **kwargs)
#         # func takes list as an input
#         if kwargs.get('run_opt', {}).get('run_list'):
#             return func(self, arg, *args, **kwargs)
#         # grep options
#         results = []
#         opt = kwargs.pop('program_opt', {})
#         run_opt_parent = kwargs.pop('run_opt', {})
#         sub_dir_parent = Path(run_opt_parent.get('sub_dir', ''))
#         sub_dir_prefix = run_opt_parent.get('sub_dir_prefix', 'calc_')
#         sub_dir_range = list(run_opt_parent.get('sub_dir_range',
#                                                 range(len(list(arg)))))
#         run_opt = deepcopy(run_opt_parent)
#         # set sub_dir names, already existing dirs handled by Runner(?)
#         sub_dirs = [sub_dir_parent /
#                     (sub_dir_prefix + f'{s}') for s in sub_dir_range]

#         if self.compute_settings.name== 'saga':
#             run_opt.update({'sub_dirs': sub_dirs})
#             cs_local = hyset.create_compute_settings('local', dry_run=True)
#             func_local = copy.deepcopy(func)
#             func.__self__.Runner = cs_local.Runner
#             commands = [func_local(self, elem, *args, program_opt=opt,
#                                    **kwargs) for elem in arg]
#             return func(arg[0],  # <- dummy
#                         *args, program_opt=opt, run_opt=run_opt,
#                         run_sequentially=True, run_command=commands)

#         if self.compute_settings.name != 'local':
#             raise NotImplementedError('run_sequentially only for local')
#         # loop over list elements
#         for i, elem in enumerate(arg):
#             run_opt.update({'sub_dir': sub_dirs[i]})
#             if isinstance(opt, list) and len(opt) == len(list(arg)):
#                 # element specific program options
#                 program_opt = opt[i]
#             else:
#                 program_opt = opt
#             results.append(func(self, elem, *args,
#                                 program_opt=program_opt,
#                                 run_opt=run_opt,
#                                 **kwargs))
#         return results
#     return wrapper

#   run_opt.update({'sub_dirs': sub_dirs,
#                   'bundle_on_saga': len(list(arg))})
#   return func(self, arg, *args, program_opt=opt, run_opt=run_opt,
#               **kwargs)
#
#
# cs_local = create_compute_settings('local', dry_run=True)
# cs_saga = create_compute_settings('saga')
# myxtb_local = hyif.Xtb({}, compute_settings=cs_local)
# myxtb_saga = hyif.Xtb({}, compute_settings=cs_saga)
#
# mols = 'O', 'S', 'F'
# mymols = [ Molecule(x) for x in mols]
# result = bundle_to_saga(myxtb_local.get_energy(),
#                         myxtb_saga.get_energy(), mymols)
#
# to avoid two functions, copy myxtb_saga and inject the copy
#  with (dry-)Runner from cs_local
#
# get_energy_local = copy.deepcopy(myxtb_saga.get_energy)
# get_energy_local.__self__.Runner = cs_local.Runner
#
#
# def bundle_to_saga(func_local, func_saga, mols):
#     commands = [ func_local(mol) for mol in mols]
#     return func_saga(mols[0], # <- dummy
#                      run_sequentially=True,
#                      run_command=commands)
#

# def run_sequentially(func):
#     """Decorator for performing a sequence of runs."""
#     @wraps(func)
#     def wrapper(self, arg, *args, **kwargs):
#         # normal case
#         if not isinstance(arg, list):
#             return func(self, arg, *args, **kwargs)
#         results = []
#         # loop over list elements
#         for i, elem in enumerate(arg):
#             # print('runnint sequential task', i)
#             results.append(func(self, elem, *args,
#                                 **kwargs))
#         return results
#     return wrapper

# def run_sequentially:
#     """Decorator for performing a sequence of runs."""
#     def inner(func):
#         @wraps(func)
#         def wrapper(self, arg, *args, **kwargs):
#             if not isinstance(arg, list):
#                 return func(self, arg, *args, **kwargs)
#             # allow list input types
#             if kwargs.get('options', {}).get('run_list'):
#                 return func(self, arg, *args, **kwargs)
#             results = []
#             opt_list = kwargs.get('options', {}).get('options_list', [])
#             if len(list(opt_list)) != len(list(arg)):
#                 opt_list = None
#             for i, elem in enumerate(arg):
#                 # allow element-specific options
#                 if opt_list:
#                     opt = opt_list[i]
#                 else:
#                     opt = kwargs.pop('options', {})
#                 # TODO: construct sub_dir name from i
#                 subdir_arg = decorator_kwargs.get('sub_dir', False)
#                 if subdir_arg:
#                     opt.update({'sub_dir': subdir_arg})
#                 results.append(func(self, elem, *args,
#                                     options=opt, **kwargs))
#             return results
#         return wrapper
#     return inner


def is_file(filename: Union[str, Path], parent: Optional[Path] = None) -> bool:
    """Check if input string is a file and exists."""
    isfile: bool
    try:
        file = Path(filename)
    except (OSError, TypeError):
        isfile = False
    else:

        if not file.is_absolute():
            if parent is not None:
                path = parent / file
            else:
                path = file.resolve()
        else:
            path = file
        try:
            isfile = path.exists()
        except (OSError, TypeError):
            isfile = False

    return isfile


def read_from_str(output: Union[str, Path]) -> Generator[str, None, None]:
    """Read rows from file or string.

    Parameters
    ----------
    output : str or :obj:`pathlib.Path`
        output filename or string

    Returns
    -------
    Generator
        file or string row

    """
    try:
        filename = Path(str(output))
        is_file = filename.exists()
    except (OSError, TypeError):
        is_file = False

    if is_file:
        for row in open(filename, 'r'):
            yield row
    else:
        for row in str(output).split('\n'):
            yield row


def update_nested_dict(d, u, exclude=[]):
    """Update (nested) dictionary d with dictionary u."""
    for k, v in u.items():
        if k in exclude:
            continue
        if isinstance(v, collections.abc.Mapping):
            d[k] = update_nested_dict(d.get(k, {}), v)
        else:
            d[k] = v
    return d


def key_in_arglist(*args, **kwargs) -> bool:
    """Look for keyword/attribute in input."""
    if 'look_for' not in kwargs:
        return False
    search_str = kwargs['look_for']

    if search_str in kwargs:
        return True

    for arg in args:
        if isinstance(arg, dict):
            if search_str in arg:
                return True
        elif inspect.isclass(type(arg)):
            if hasattr(arg, search_str):
                return True
    return False


def get_val_from_arglist(*args, **kwargs) -> Any:
    """Return value of keyword/attribute."""
    if 'look_for' not in kwargs:
        return None
    search_str = kwargs['look_for']

    if search_str in kwargs:
        return kwargs[search_str]
    for arg in args:
        if isinstance(arg, dict):
            if search_str in arg:
                return arg[search_str]
        elif inspect.isclass(type(arg)):
            if hasattr(arg, search_str):
                return getattr(arg, search_str)
    return None


def unique_filename(input_strings: List[str]) -> str:
    """Create a unique filename based on a list of input strings.

       Courtesy of daltonproject.

    Parameters
    ----------
    input_strings: List[str]
        List of input strings.

    Returns
    -------
    str:
        Unique filename.

    """
    import hashlib
    if not isinstance(input_strings, list):
        raise TypeError(f'Expected list, got {type(input_strings).__name__}.')
    for i, input_string in enumerate(input_strings):
        if not isinstance(input_string, str):
            e = f'item {i}: expected str got, {type(input_string).__name__}.'
            raise TypeError(e)
    return hashlib.sha1(''.join(input_strings).encode('utf-8')).hexdigest()


def update_arglist(args_old: List[Union[str, dict]],
                   args_new: List[Union[str, dict]]) -> List[Union[str, dict]]:
    """Update args."""
    args = copy.deepcopy(args_old)
    for x in args_new:
        if isinstance(x, str):
            if x not in args:
                args.append(x)
        elif isinstance(x, dict):
            if len(x) > 1:
                raise ValueError(
                    'Only one key-value pair is allowed in a dict')
            k, v = list(x.items())[0]
            found = False
            for i, y in enumerate(args):
                if isinstance(y, dict):
                    if k in y:
                        found = True
                        y[k] = v
                elif isinstance(y, str):
                    if k == y:
                        found = True
                        args[i + 1] = v
            if not found:
                args.append(x)
    return args
