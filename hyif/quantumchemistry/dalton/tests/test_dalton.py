import numpy as np
from hyobj import Molecule

from hyif import Dalton


def test_dalton():
    """Testing Dalton interface."""
    mymol = Molecule(''' O   0.0000   0.0000   0.0626
 H  -0.7920   0.0000  -0.4973
 H   0.7920   0.0000  -0.4973''')
    ref_energy = -76.370937598093
    ref_gradient = np.array([
            [0., -0., -0.00868404],
            [-0.00801714, -0., 0.00433757],
            [0.00801714, -0., 0.00433757]])

    mydalton = Dalton({})
    out = mydalton.run(mymol)
    np.testing.assert_almost_equal(out.energy, ref_energy)
    en = mydalton.get_energy(mymol)
    np.testing.assert_almost_equal(en, ref_energy)
    grad = mydalton.get_gradient(mymol)
    np.testing.assert_allclose(grad, ref_gradient, rtol=1e-6, atol=1e-6)
