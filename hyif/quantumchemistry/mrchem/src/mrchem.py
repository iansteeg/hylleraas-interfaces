from __future__ import annotations

import copy
import warnings
from dataclasses import replace
from typing import Optional

import numpy as np
import packaging.version as pv
from hyobj import Units
from hyset import (ComputeResult, ComputeSettings, RunSettings,
                   create_compute_settings, run_sequentially)

from ....manager import File, NewRun
from ....utils import unique_filename
from ...abc import HylleraasQMInterface, Runner
from ...molecule import Molecule
from .parser import MRChemInput, MRChemOutput


class MRChem(HylleraasQMInterface, NewRun):
    """MRChem interface."""

    def __init__(
        self, method: dict, compute_settings: Optional[ComputeSettings] = None
    ):
        self.method = method

        if compute_settings is None:
            self.compute_settings = create_compute_settings()
        else:
            self.compute_settings = compute_settings

        self.runner: Runner = self._set_runner(self.compute_settings)
        self.inputParser = MRChemInput
        self.outputParser = MRChemOutput

        self.version: pv.Version = (
            self.check_version()
            if self.method.get('check_version',
                               self.compute_settings.arch_type == 'local')
            else None
        )

        self.run_settings = self.compute_settings.run_settings
        self.units = self.method.get('units', Units())

    def _set_runner(self, compute_settings: ComputeSettings) -> Runner:
        """Set runner.

        Parameter
        ---------
        compute_settings: :obj:`ComputeSettings`
            current compute settings

        Returns
        -------
        :obj:`Runner`
            runner for performing calculations

        """
        # return getattr(
        #     self.compute_settings,
        #     str(SETTINGS[self.compute_settings.name]['runner']),
        # )
        pass

    def check_version(self) -> pv.Version:
        """Check if program version is compatible with interface.

        Returns
        -------
        :obj:`packaging.Version'
            version object

        """
        versions_supported = ['1.1.0a0', '1.1.1', '1.1.3', '1.2.0a0']

        version: pv.Version = None
        if self.compute_settings.name == 'local':
            version_test_run_settings = copy.deepcopy(self.compute_settings)
            version_test_run_settings.run_settings = replace(
                version_test_run_settings.run_settings,
                program='mrchem',
                args=['--version'],
                launcher=[]
            )
            output = self.compute_settings.Runner.run(
                version_test_run_settings.run_settings,
            )
            version = pv.parse(str(output.stdout))

        if version not in (pv.parse(v) for v in versions_supported):
            raise NotImplementedError(
                f' MRChem version {version} not supported.',
                f' Please contact {self.author} ',
            )
        return version

    @property
    def author(self):
        """Return author's email adress."""
        return 'bin.gao@uit.no'

    @run_sequentially
    def parse(self, output: ComputeResult, **kwargs) -> dict:
        """Parse output."""
        outfile = output.output_file
        result = {}
        result['stdout'] = output.stdout
        result['stderr'] = output.stderr
        if outfile is not None:
            result.update(self.outputParser.parse(outfile))
            result['output_file'] = str(outfile)

            if result['warnings']:
                warnings.warn('\n'.join(result['warnings']))
            if result['errors']:
                raise RuntimeError('\n'.join(result['errors']))

        return result

    @run_sequentially
    def setup(self, molecule: Molecule, **kwargs) -> RunSettings:
        """Set up calculation."""
        run_settings = copy.deepcopy(self.run_settings)
        if 'run_opt' in kwargs.keys():
            run_settings = replace(run_settings, **kwargs.pop('run_opt'))

        if molecule is None:
            return run_settings

        # Set input file
        input_str = self.inputParser.gen_mrchem_input(self.method, molecule)
        filename = unique_filename(input_str.split('\n'))
        input_file = File(name=filename + '.inp', content=input_str)
        files_to_write = [input_file]

        # Set up options, in the format of
        # "--launcher='...'", "--executable='mrchem.x'", "--stdout", "--json"
        args = ["--executable='mrchem.x'", '--stdout', '--json']
        if len(run_settings.launcher) > 0:
            # FIXME: is ''.join(...) correct for every case?
            args.append("--launcher='"+''.join(run_settings.launcher)+"'")
        args.append(filename)

        # Results will be extracted from the JSON file, while the standard
        # output from MRChem will be redirected to .out file
        json_file = File(name=filename + '.json')
        output_file = File(name=filename + '.out')

        running_dict = {
            'program': 'mrchem',
            'args': args,
            'output_file': json_file,
            'stdout_redirect': output_file,
            'files_to_write': files_to_write,
            'launcher': ''
        }

        return replace(run_settings, **running_dict)

    @run_sequentially
    def get_energy(
        self, molecule: Molecule, options: Optional[dict] = {}
    ) -> float:
        """Compute energy with MRChem.

        Paramters
        ---------
        molecule : :obj:`Molecule` (hylleraas-compatible)
            molecule for which energy should be computed

        Returns
        -------
        float
            energy

        """
        result = self.run(molecule, options=options)
        return result['energy']

    @run_sequentially
    def get_gradient(
        self,
        molecule: Molecule,
        options: Optional[dict] = {}
    ) -> np.array:
        """Compute Gradient.

        Paramters
        ---------
        molecule : :obj:`Molecule` (hylleraas-compatible)
            molecule for which energy should be computed

        Returns
        -------
        np.ndarray
            gradient in unit Hartee/(Molecule.properties['unit'])

        """
        result = self.run(molecule, options=options)
        return result['gradient']

    @run_sequentially
    def get_hessian(
        self,
        molecule: Molecule,
        options: Optional[dict] = {}
    ) -> np.array:
        """Compute Hessian."""
        raise RuntimeError('Hessian is not implemented in MRChem interface')

    @run_sequentially
    def get_nmr_shielding(
        self, molecule: Molecule, options: Optional[dict] = {}
    ) -> dict[int, dict[str, np.array]]:
        """Compute NMR shielding tensors with MRChem.

        Paramters
        ---------
        molecule : :obj:`Molecule` (hylleraas-compatible)
            molecule for which energy should be computed

        Returns
        -------
        dict
            NMR shielding tensors, with key being the index of each atom
            and starting from 0

        """
        result = self.run(molecule, options=options)
        return result['nmr_shielding']
