import warnings
from abc import ABC, abstractmethod
from pathlib import Path
from typing import Dict, List, Union

import numpy as np


class XtbOutput(ABC):
    """XTB outpur parser class."""

    @abstractmethod
    def parse(self, str):
        """Parse prototype."""


class Xtbexternal(XtbOutput):
    """Class for parsing non-output files."""

    KEYS = {
        '* xtb version': ('version', 3, str),
        'Cite this work as': ('cite', 1, str, 3, 'DOI: '),
        'Reference': ('Reference', 1, str),
        ':: total energy': ('energy', 3, float),
        'HOMO-LUMO gap': ('homo_lumo_gap', 3, float),
        '# basis functions': ('basis_functions', 4, int),
        '# atomic orbitals': ('atomic_orbitals', 4, int),
        '# electrons': ('electrons', 3, int),
        'convergence criteria satisfied after': ('iterations', 5, int),
        'GEOMETRY OPTIMIZATION CONVERGED AFTER': (
            'geometry_iterations', 5, int),
        'omp threads': ('omp_threads', 3, str),
        'Fermi-level': ('fermi_level', 3, float),
        'molecular dipole:': ('molecular_dipole', 4, float, 3),
        'GRADIENT NORM': ('gradient_norm', 3, str)
    }

    @classmethod
    def parse_output(cls, path) -> dict:
        """Parse output from file."""
        try:
            string = path.read_text()
        except UnicodeDecodeError:
            return {}

        result: dict = {}
        lines = string.split('\n')

        for i, line in enumerate(lines):
            cls.parse_line(i, line, lines, result)

        return result

    @classmethod
    def parse_line(cls, i, line, lines, result):
        """Parse line."""
        for key, value in cls.KEYS.items():
            result_key, index, type_func = value[:3]
            index = int(index)
            offset = int(value[3]) if len(value) > 3 else 0
            split_key = str(value[4]) if len(value) > 4 else None
            if key in line:
                if split_key:
                    result[result_key] = type_func(
                        lines[i + offset].split(split_key)[index])
                else:
                    result[result_key] = type_func(
                        lines[i + offset].split()[index])

        # Handle special cases
        if 'Orbital Energies and Occupations' in line:
            result.update(
                cls.parse_orbital_energies_and_occupations(i, lines))
        if 'covCN' in line:
            result.update(
                cls.parse_covcn(i, lines))
        if 'molecular quadrupole' in line:
            result.update(
                cls.parse_molecular_quadrupole(i, lines))
        if 'TOTAL ENERGY' in line:
            cls.parse_total_energy(i, lines, result)
        if 'wall-time' in line:
            cls.parse_wall_time(i, lines)

    @classmethod
    def parse_orbital_energies_and_occupations(cls,
                                               i: int,
                                               lines: List[str]) -> dict:
        """Parse orbital energies and occupations."""
        result: Dict[str, Union[List, int]] = {}
        ens: list = []
        occs: list = []
        homo: int = -2
        lumo: int = -2
        j = i + 4
        while j <= len(lines):
            if '... ' in lines[j]:
                j += 1
                continue
            if '---' in lines[j]:
                break
            # print(lines[j])
            orb_line = lines[j].split()
            num = int(orb_line[0])
            en = float(orb_line[2])
            if len(orb_line) > 3:
                if len(orb_line) == 4 and '(LUMO)' in orb_line:
                    occ = 0.0
                else:
                    occ = float(orb_line[1])
            else:
                occ = 0.0
            for _ in range(len(ens), num - 1):
                occs.append(occ)
                ens.append(1e6)
            ens.append(en)
            occs.append(occ)
            if '(HOMO)' in lines[j]:
                homo = num
            if '(LUMO)' in lines[j]:
                lumo = num
            j += 1
        result['orbital_energies'] = ens
        result['orbital_occupations'] = occs
        result['orbital_homo'] = homo
        result['orbital_lumo'] = lumo
        return result

    @classmethod
    def parse_covcn(cls, i: int, lines: List[str]) -> dict:
        """Parse coordinateion numbers."""
        result = {}
        coordination_numbers = []
        for j in range(i, len(lines)):
            if 'Mol.' in lines[j]:
                break

            cn = lines[j].split()
            if len(cn) != 7:
                continue
            coordination_numbers.append(float(cn[3]))
        result['coordination_numbers'] = coordination_numbers
        return result

    @classmethod
    def parse_molecular_quadrupole(cls, i: int, lines: List[str]) -> dict:
        """Parse molecular quadrupole."""
        result = {}
        result['molecular_quadrupole'] = np.array(
            lines[i + 4].split()[1:])
        result['molecular_quadrupole_order'] = ['xx', 'xy', 'yy',
                                                'xz', 'yz', 'zz']
        return result

    @classmethod
    def parse_total_energy(cls, i: int, lines: List[str], ref: dict) -> dict:
        """Parse total energy."""
        result = {}
        result['total_energy'] = float(lines[i].split()[3])
        if 'energy' not in ref:
            result['energy'] = result['total_energy']
        return result

    @classmethod
    def parse_wall_time(cls, i: int, lines: List[str]) -> dict:
        """Parse wall time."""
        result = {}
        for j in range(i, len(lines)):
            if 'wall-time' in lines[j]:
                module = lines[i - 1].strip().split(':')[0]
                result[f'{module}_wall_time'] = cls.calculate_time(
                    *map(float, lines[j].split()[2::2]))
                result[f'{module}_cpu_time'] = cls.calculate_time(
                    *map(float, lines[j + 1].split()[2::2]))
        return result

    @classmethod
    def read_scan(cls, path):
        """Read scan from file."""
        if not path.exists():
            warnings.warn('could not find scan file {path}')
        energies = []
        coordinates = []
        lines = path.read_text().split('\n')
        n = int(lines[0].strip())
        for i, line in enumerate(lines):
            if 'energy' in line:
                energies.append(float(line.split()[1]))
                coordinates.append([])
                for j in range(i + 1, i + 1 + n):
                    coordinates[-1].append([float(x)
                                            for x in lines[j].split()[1:]])
        return {'energies': energies, 'coordinates': coordinates}

    @classmethod
    def read_final_geometry(cls, path):
        """Read final geometry from file."""
        if not path.exists():
            warnings.warn('could not find converged geometry {path}')
        xyz = path.read_text()
        lines = xyz.split('\n')
        n = int(lines[0].strip())
        atoms = []
        coordinates = []
        for j in range(2, 2 + n):
            atom, x, y, z = lines[j].split()
            atoms.append(atom)
            coordinates.append([float(x), float(y), float(z)])
        return {'xyz': xyz, 'atoms': atoms, 'coordinates': coordinates}

    @classmethod
    def read_gradient(cls, path):
        """Read gradient from file."""
        if not path.exists():
            warnings.warn('could not find gradient file {path}')
        with open(path, 'r') as grad_file:
            lines = grad_file.read().split('\n')
        gradient = []
        num_atoms = 0
        for i, line in enumerate(lines):
            if 'Number of atoms' in line:
                num_atoms = int(lines[i + 2])
            if 'The current gradient in Eh/bohr' in line:
                for j in range(i + 2, i + 2 + 3 * num_atoms):
                    gradient.append(float(lines[j]))

        return np.array(gradient)

    @classmethod
    def read_hessian(cls, path):
        """Read hessian from file."""
        if not path.exists():
            warnings.warn(f'could not find hessian file {path}')
        with open(path, 'r') as hessian_file:
            lines = hessian_file.read().split('\n')

        del lines[0]

        lines = [line.split() for line in lines if line]
        hessian = []
        for line in lines:
            for val in line:
                hessian.append(float(val))

        dim = int(np.sqrt(len(hessian)))
        return np.array(hessian).reshape(dim, dim)

    @classmethod
    def read_frequencies(cls, path):
        """Read vibrational frequencies from file."""
        if not path.exists():
            warnings.warn('could not find g98.out file {path}')
        with open(path, 'r') as file:
            lines = file.read().split('\n')

        frequencies = []
        for i, line in enumerate(lines):
            if 'Frequencies --' in line:
                freqs = lines[i].split()[2:]
                for freq in freqs:
                    frequencies.append(float(freq))

        return frequencies

    @classmethod
    def read_normal_modes(cls, path):
        """Read normal modes from file."""
        if not path.exists():
            warnings.warn('could not find g98.out file {path}')
        with open(path, 'r') as file:
            lines = file.read().split('\n')

        num_atoms = 0
        num_modes = 0
        for i, line in enumerate(lines):
            if 'basis functions' in line:
                num_atoms = int(lines[i - 2].split()[0])
            if 'Atom AN      X      Y      Z' in line:
                row = lines[i + 2].split()[2:]
                num_modes += len(row) // 3

        normal_modes = np.empty((num_modes, 3 * num_atoms))
        modes_tot = 0
        for i, line in enumerate(lines):
            if 'Atom AN      X      Y      Z' in line:
                row = lines[i + 2].split()[2:]
                num_modes_line = len(row) // 3
                for j in range(num_modes_line):
                    idx_mode = modes_tot + j
                    mode_start = 2 + 3 * j
                    mode_end = 2 + 3 * j + 3
                    for k in range(num_atoms):
                        row = lines[i + 1 + k].split()[mode_start:mode_end]
                        row = [float(x) for x in row]
                        row = np.array(row)
                        normal_modes[idx_mode, 3 * k:3 * k + 3] = row[:]
                modes_tot += num_modes_line

        return normal_modes

    @classmethod
    def calculate_time(cls,
                       day: float, hour: float,
                       minutes: float, sec: float) -> float:
        """Calculate time."""
        return sec + 60.0 * (minutes + 60.0 * (hour + 24.0 * day))

    # @classmethod
    # def parse_output_old(cls, path) -> dict:
    #     """Parse output from file."""
    #     try:
    #         string = path.read_text()
    #     except UnicodeDecodeError:
    #         return {}

    #     result: dict = {}
    #     lines = string.split('\n')

    #     for i, line in enumerate(lines):
    #         if '* xtb version' in line:
    #             result['version'] = line.split()[3]
    #         if 'Cite this work as' in line:
    #             result['cite'] = lines[i + 3].split('DOI: ')[1]
    #         if 'Reference' in line:
    #             result['Reference'] = line.split()[1]
    #         if ':: total energy' in line:
    #             result['energy'] = float(line.split()[3])
    #         if 'HOMO-LUMO gap' in line:
    #             result['homo_lumo_gap'] = float(line.split()[3])
    #         if '# basis functions' in line:
    #             result['basis_functions'] = int(line.split()[4])
    #         if '# atomic orbitals' in line:
    #             result['atomic_orbitals'] = int(line.split()[4])
    #         if '# electrons' in line:
    #             result['electrons'] = int(line.split()[3])
    #         if 'convergence criteria satisfied after' in line:
    #             result['iterations'] = int(line.split()[5])
    #         if 'GEOMETRY OPTIMIZATION CONVERGED AFTER' in line:
    #             result['geometry_iterations'] = int(line.split()[5])
    #         if 'omp threads' in line:
    #             result['omp_threads'] = line.split()[3]
    #         if 'Fermi-level' in line:
    #             result['fermi_level'] = float(line.split()[3])
    #         if 'molecular dipole:' in line:
    #             result['molecular_dipole'] = float(lines[i + 3].split()[4])
    #         if 'GRADIENT NORM' in line:
    #             result['gradient_norm'] = line.split()[3]
    #         if 'Hamiltonian' in line:
    #             if len(line.split()) == 4:
    #                 result['Hamiltonian'] = line.split()[2]
    #         if 'Orbital Energies and Occupations' in line:
    #             ens: list = []
    #             occs: list = []
    #             homo: int = -2
    #             lumo: int = -2
    #             j = i + 4
    #             while j <= len(lines):
    #                 if '... ' in lines[j]:
    #                     j += 1
    #                     continue
    #                 if '---' in lines[j]:
    #                     break
    #                 # print(lines[j])
    #                 orb_line = lines[j].split()
    #                 num = int(orb_line[0])
    #                 en = float(orb_line[2])
    #                 if len(orb_line) > 3:
    #                     if len(orb_line) == 4 and '(LUMO)' in orb_line:
    #                         occ = 0.0
    #                     else:
    #                         occ = float(orb_line[1])
    #                 else:
    #                     occ = 0.0
    #                 for k in range(len(ens), num - 1):
    #                     occs.append(occ)
    #                     ens.append(1e6)
    #                 ens.append(en)
    #                 occs.append(occ)
    #                 if '(HOMO)' in lines[j]:
    #                     homo = num
    #                 if '(LUMO)' in lines[j]:
    #                     lumo = num
    #                 j += 1
    #             result['orbital_energies'] = ens
    #             result['orbital_occupations'] = occs
    #             result['orbital_homo'] = homo
    #             result['orbital_lumo'] = lumo
    #         # if 'HL-Gap' in line:
    #         #     result['HL-Gap'] = float(line.split()[1])

    #         if 'covCN' in line:
    #             coordination_numbers = []
    #             for j in range(i, len(lines)):
    #                 if 'Mol.' in lines[j]:
    #                     break

    #                 cn = lines[j].split()
    #                 if len(cn) != 7:
    #                     continue
    #                 coordination_numbers.append(float(cn[3]))

    #             result['coordination_numbers'] = coordination_numbers

    #         # if 'Wiberg/Mayer (AO) data.' in line:
    #         #     wbo = np.zeros((self.num_atoms, self.num_atoms))
    #         #     for j in range(i + 6, len(lines)):
    #         #         if self.num_atoms <= 1:
    #         #             break
    #         #         if '---' in lines[j]:
    #         #             break
    #         #         split_line = lines[j].split()
    #         #         if '--' in lines[j]:
    #         #             idx1 = int(split_line[0]) - 1
    #         #             nconn = (len(split_line) - 5) // 3
    #         #             for k in range(nconn):
    #         #                 idx2 = int(split_line[5 + k*3]) - 1
    #         #                 val = float(split_line[5 + k*3 + 2])
    #         #                 wbo[idx1, idx2] = val
    #         #         else:
    #         #             nconn = (len(split_line)) // 3
    #         #             for k in range(nconn):
    #         #                 idx2 = int(split_line[k * 3]) - 1
    #         #                 val = float(split_line[k*3 + 2])
    #         #                 wbo[idx1, idx2] = val

    #         #     result['wiberg_bond_orders'] = wbo

    #         if 'molecular quadrupole' in line:
    #             result['molecular_quadrupole'] = np.array(
    #                 lines[i + 4].split()[1:])
    #             result['molecular_quadrupole_order'] = ['xx', 'xy', 'yy'
    #                                                     'xz', 'yz', 'zz']

    #         if 'TOTAL ENERGY' in line:
    #             result['total_energy'] = float(line.split()[3])
    #             if 'energy' not in result:
    #                 result['energy'] = result['total_energy']

    #         if 'wall-time' in line:
    #             module = lines[i - 1].strip().split(':')[0]
    #             result[f'{module}_wall_time'] = cls.calculate_time(
    #                 *map(float, lines[i].split()[2::2]))
    #             result[f'{module}_cpu_time'] = cls.calculate_time(
    #                 *map(float, lines[i + 1].split()[2::2]))
    #     return result


class XtbFast(XtbOutput):
    """Class for parsing output from string."""

    def dummy_parser(self, *args, **kwargs):
        """Mimick dummy parser."""
        return {}

    def parse(self, output) -> dict:
        """Parse output-file or folder."""
        result: dict = {}

        try:
            f = Path(output)
        except (OSError, TypeError):
            raise FileNotFoundError(f'could not find file {output}')
        else:
            if not f.exists():
                raise FileNotFoundError(f'could not find file {output}')
            if f.is_dir():
                for ff in f.iterdir():
                    result.update(self.parse(ff))
                return result

        if 'xtbopt.xyz' in f.name:
            d = Xtbexternal.read_final_geometry(f)
            result['xtbopt.xyz'] = d['xyz']
            result['final_geometry'] = d['coordinates']
        elif f.name.endswith('.xyz') and 'xtbopt' not in f.name:
            d = Xtbexternal.read_final_geometry(f)
            result['input_geometry'] = d['coordinates']
            result['atoms'] = d['atoms']
        elif f.name.endswith('.engrad'):
            result['gradient'] = Xtbexternal.read_gradient(f)
        elif 'hessian' in f.name:
            result['hessian'] = Xtbexternal.read_hessian(f)
        elif 'g98.out' in f.name:
            result['frequencies'] = Xtbexternal.read_frequencies(f)
            result['normal_modes'] = Xtbexternal.read_normal_modes(f)
        elif 'xtbscan.log' in f.name:
            result['scan'] = Xtbexternal.read_scan(f)
        elif f.name.endswith('.trj'):
            result['trajectories'] = Xtbexternal.read_scan(f)
        elif f.name.endswith('.out'):
            result.update(Xtbexternal.parse_output(f))

        return result
