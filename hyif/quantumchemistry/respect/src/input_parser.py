from pathlib import Path

import yaml


class InputParser:
    """Parse input file."""

    def parse(self, file: Path) -> dict:
        """Parse input file.

        Parameters
        ----------
        file : Path
            path to input file

        Returns
        -------
        dict
            dictionary with parsed input

        """
        text: str
        try:
            path = Path(file)
        except (OSError, TypeError):
            text = str(file)
        else:
            if path.exists():
                text = path.read_text()
            else:
                text = str(file)

        text.replace('geometry:', 'geometry: |')
        d = yaml.safe_load(text)
        # paranoia
        if d.get('scf', {}).get('geometry', None) is not None:
            xyz = d['scf']['geometry'].split()
            #     natoms = len(xyz)//4
            xyz = [xyz[i:i + 4] for i in range(0, len(xyz), 4)]
            xyz = ' '.join([' '.join(x) for x in xyz])
            # format : S 0.000000 0.000000 0.000000 H 0.000000 0.000000\
            # 1.336000 H 1.335103 0.000000 -0.048956
            # atoms = [x.pop(0) for x in xyz]
            # xyz = [[float(x) for x in y] for y in xyz]
            # print(atoms, xyz)
            # lmmk
            #     for atom in atoms:
            #         d['scf']['geometry']

            # xyz = [' '.join(xyz[i:i+4]) for i in range(0, len(xyz), 4)]
            # #     xyz = '\n'.join(xyz)
            #     # for atom in atoms:

            d['scf']['geometry'] = xyz
        return d

    def gen_input_str(self, input_dict):
        """Generate text file input."""
        indent = 2 * ' '
        yaml_string = yaml.dump(input_dict, sort_keys=False)
        if 'geometry:' in yaml_string:
            lines = yaml_string.splitlines()
            for i, line in enumerate(lines):
                if 'geometry:' in line:
                    line_split = line.split()
                    if ':' not in lines[i + 1]:
                        line_split += lines[i + 1].split()
                        lines.pop(i + 1)
                    lines[i] = indent + line_split.pop(0)
                    line_split = [
                        ' '.join(line_split[i:i + 4])
                        for i in range(0, len(line_split), 4)
                    ]
                    line_split = [2 * indent + ll for ll in line_split]
                    for j, ll in enumerate(line_split):
                        lines.insert(i + 1 + j, ll)
                    break

            return '\n'.join(lines)
        else:
            return yaml_string


# if __name__ == '__main__':

#     parser = InputParser()
#     d = parser.parse(
#         '/Users/tilmann/Documents/work/hylleraas/hyif/hyif/quantumchemistry/respect/test/template.inp'
#     )
#     print(d['scf']['geometry'])
#     # yaml_string=yaml.dump(d,sort_keys=False)
#     # xyz =
#     # print(yaml_string)
#     print(out)
