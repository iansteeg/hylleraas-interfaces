import copy
import inspect
import json
import warnings
from dataclasses import replace
from functools import singledispatchmethod
from pathlib import Path
from typing import Any, Dict, List, Optional, Union

import numpy as np
import packaging.version as pv

try:
    import pandas as pd

    df_type = pd.DataFrame
except ImportError:
    pd = None
    df_type = Any

from hyobj import DataSet
from hyset import (ComputeResult, ComputeSettings, File, RunSettings,
                   create_compute_settings)

from ....manager import acompute, compute
from ....utils import is_file, unique_filename
from ...abc import HylleraasMLInterface, Parser
from ...common import Common
from .data_structure import DeepMDData
from .input_parser import InputParser
from .units import DPUnits

VERSIONS_SUPPORTED = ['2.1.5', '2.2.1']

data_type = Union[DeepMDData, DataSet, str, Path]

DEFAULT_INPUT = {
    '_comment': "that's all",
    'model': {
        'descriptor': {
            'type': 'se_e2_a',
            'rcut_smth': 0.5,
            'rcut': 6.0,
            'neuron': [25, 50, 100],
            'resnet_dt': False,
            'axis_neuron': 16,
            'seed': 1,
            '_comment': " that's all",
        },
        'fitting_net': {
            'neuron': [240, 240, 240],
            'resnet_dt': True,
            'seed': 1,
            '_comment': " that's all",
        },
        '_comment': " that's all",
    },
    'learning_rate': {
        'type': 'exp',
        'decay_steps': 5000,
        'start_lr': 0.001,
        'stop_lr': 3.51e-08,
        '_comment': "that's all",
    },
    'loss': {
        'type': 'ener',
        'start_pref_e': 0.02,
        'limit_pref_e': 1,
        'start_pref_f': 1000,
        'limit_pref_f': 1,
        'start_pref_v': 0,
        'limit_pref_v': 0,
        '_comment': " that's all",
    },
    'training': {
        'training_data': {
            'systems': [],
            'batch_size': 'auto',
            '_comment': "that's all",
        },
        'numb_steps': 1000,
        'seed': 10,
        'disp_file': 'lcurve.out',
        'disp_freq': 100,
        'save_freq': 1000,
        '_comment': "that's all",
    },
}


class DeepMD(HylleraasMLInterface, Common):
    """Interface to DeePMD."""

    def __init__(
        self, method: dict, compute_settings: Optional[ComputeSettings] = None
    ):
        if compute_settings is None:
            self.compute_settings = create_compute_settings()
        else:
            self.compute_settings = compute_settings

        self.method = method
        self.Runner = self.compute_settings.Runner
        self.InputParser: Parser = InputParser()  # type: ignore

        self.input: str = method.get('input', DEFAULT_INPUT)

        self.arch_type = getattr(self.compute_settings, 'arch_type', 'remote')

        # TODO: implement restart
        files_for_restarting: List[Path] = [
            # self.run_settings.abs_path('checkpoint'),
            # self.run_settings.abs_path('lcurve.out'),
        ]

        defaults = {
            'program': 'dp',
            'files_for_restarting': files_for_restarting,
        }
        self.run_settings = replace(
            self.compute_settings.run_settings, **defaults
        )

        self.version: pv.Version = (
            self.check_version()
            if self.method.get(
                'check_version', self.compute_settings.arch_type == 'local'
            )
            else None
        )

        self.debug = self.method.get('debug', False)

        self.type_map = self.method.get('type_map', None)
        self.units = self.method.get('units', DPUnits)

    @singledispatchmethod
    def get_data(self, data, **kwargs) -> DeepMDData:
        """Get data from input."""
        raise TypeError('data must be a DeepMDData, a DataSet, dict or a path')

    @get_data.register(Path)
    @get_data.register(str)
    def _(self, data: Union[str, Path], **kwargs):
        """Get data from input."""
        if not Path(data).is_dir():
            raise ValueError(f'data {data} must be a directory')

        return DeepMDData(data_dir=data, **kwargs)

    @get_data.register(DeepMDData)
    def _(self, data: DeepMDData, **kwargs):
        """Get data from input."""
        # if kwargs.get('dump', False):
        #     path = kwargs.get('path', data.path)
        #     data.dump(path=path)
        return data

    @get_data.register(DataSet)
    @get_data.register(df_type)
    @get_data.register(dict)
    def _(self, data: Union[DataSet, dict], **kwargs):
        if isinstance(data, DataSet):
            data = data.data.to_dict(orient='list')
        elif isinstance(data, df_type):
            data = data.to_dict(orient='list')
        dp_data_dict = {}

        for k, v in data.items():
            if k in ['coord', 'coordinates']:
                dp_data_dict['coordinates'] = np.array(v)
            if k in ['energy', 'energies']:
                dp_data_dict['energies'] = np.array(v)
            if k in ['force', 'forces', 'gradient', 'gradients', 'grad']:
                forces = np.array(v)
                if k.startswith('grad'):
                    forces = -forces
                dp_data_dict['forces'] = forces
            if k in ['box', 'boxes', 'unit_cell', 'pbc']:
                dp_data_dict['boxes'] = np.array(v)
            if k in ['virial', 'virials']:
                dp_data_dict['virials'] = np.array(v)
            if k in ['atom_types', 'real_atom_types']:
                dp_data_dict['real_atom_types'] = v
            if k in ['type_raw']:
                dp_data_dict['type_raw'] = v
            if k in ['type_map_raw']:
                dp_data_dict['type_map_raw'] = v
            if k in ['atoms']:
                dp_data_dict['atoms'] = v

        return DeepMDData(**dp_data_dict, **kwargs)

    @singledispatchmethod
    def dump_data(self, data, **kwargs) -> None:
        """Dump data to file."""
        raise TypeError('data must be a DeepMDData')

    @dump_data.register(DeepMDData)
    def _(self, data: DeepMDData, **kwargs):
        """Dump data to file."""
        data.dump_data(**kwargs)

    def check_version(self):
        """Check deepmd version."""
        version = None
        with compute(
            method=self,
            object=None,  # type: ignore
            run_opt={'program': 'dp', 'args': '--version'},
        ) as r:
            version = pv.parse(r['version'])
        if version not in (pv.parse(v) for v in VERSIONS_SUPPORTED):
            raise NotImplementedError(
                f' DeePMD version {version} not supported.',
                f' Please contact {self.author} ',
            )
        return version

    def train(self, data: data_type, **kwargs) -> dict:
        """Train model.

        Arguments
        ---------
        data (data_type): The training data.
        **kwargs: Additional keyword arguments.

        Returns
        -------
        dict: The output of the training.

        """
        run_opt = kwargs.pop('run_opt', {})
        setup_train = self.setup(data, train=True, run_opt=run_opt, **kwargs)
        transfer = DeepMDData(output_file=setup_train.output_file.name)
        setup_freeze = self.setup(transfer, freeze=True, run_opt=run_opt)
        setup_freeze.data_files = []
        transfer.model = setup_freeze.output_file.name
        setup_compress = self.setup(transfer, compress=True, run_opt=run_opt)
        setup_compress.data_files = []

        output_unparsed = self.compute_settings.run(
            [setup_train, setup_freeze, setup_compress]
        )

        output: Dict[str, Any] = dict()
        train = self.parse(output_unparsed[0])
        freeze = self.parse(output_unparsed[1])
        compress = self.parse(output_unparsed[2])

        not_converged = False
        if 'is_converged' in train.keys():
            if not all(train['is_converged']):
                output.update({'is_converged': [False]})
                not_converged = True

        if not not_converged:
            output.update({'train': train})

        not_converged = False
        if 'is_converged' in freeze.keys():
            if not all(freeze['is_converged']):
                output.update({'is_converged': [False]})
                not_converged = True

        if not not_converged:
            output.update({'freeze': freeze, 'model': freeze['model']})

        not_converged = False
        if 'is_converged' in compress.keys():
            if not all(compress['is_converged']):
                output.update({'is_converged': [False]})
                not_converged = True
        if not not_converged:
            model_compressed = compress.get('model_compressed', None)
            self.type_map = self.get_type_map_from_input(train)
            output.update(
                {
                    'compress': compress,
                    'model_compressed': model_compressed,
                    'type_map': self.type_map,
                }
            )

        if self.debug:
            print(f'TRAINING RESULTS:\n {output}')

        return output

    async def atrain(self, data: data_type, **kwargs) -> dict:
        """Train model asynchronously.

        Arguments
        ---------
        data (data_type): The training data.
        **kwargs: Additional keyword arguments.

        Returns
        -------
        dict: The output of the training.

        """
        run_opt = kwargs.pop('run_opt', {})
        setup_train = self.setup(data, train=True, run_opt=run_opt, **kwargs)
        transfer = DeepMDData(output_file=setup_train.output_file.name)
        setup_freeze = self.setup(transfer, freeze=True, run_opt=run_opt)
        setup_freeze.data_files = []
        transfer.model = setup_freeze.output_file.name
        setup_compress = self.setup(transfer, compress=True, run_opt=run_opt)
        setup_compress.data_files = []

        output_unparsed = await self.compute_settings.arun(
            [setup_train, setup_freeze, setup_compress]
        )

        output: Dict[str, Any] = dict()
        train = self.parse(output_unparsed[0])
        freeze = self.parse(output_unparsed[1])
        compress = self.parse(output_unparsed[2])

        not_converged = False
        if 'is_converged' in train.keys():
            if not all(train['is_converged']):
                output.update({'is_converged': [False]})
                not_converged = True

        if not not_converged:
            output.update({'train': train})

        not_converged = False
        if 'is_converged' in freeze.keys():
            if not all(freeze['is_converged']):
                output.update({'is_converged': [False]})
                not_converged = True

        if not not_converged:
            output.update({'freeze': freeze, 'model': freeze['model']})

        not_converged = False
        if 'is_converged' in compress.keys():
            if not all(compress['is_converged']):
                output.update({'is_converged': [False]})
                not_converged = True
        if not not_converged:
            model_compressed = compress.get('model_compressed', None)
            self.type_map = self.get_type_map_from_input(train)
            output.update(
                {
                    'compress': compress,
                    'model_compressed': model_compressed,
                    'type_map': self.type_map,
                }
            )

        if self.debug:
            print(f'TRAINING RESULTS:\n {output}')
        return output

    def train_seq(self, data: data_type, **kwargs) -> dict:
        """Train model."""
        warnings.warn(
            "The 'train_seq' method is deprecated," ' use train instead.',
            DeprecationWarning,
        )
        return self.train(data, **kwargs)

    def test(self, data, **kwargs):
        """Test model."""
        result = self.run(data, test=True, **kwargs)
        return result

    def setup(
        self, data: Union[list, DataSet, DeepMDData, str, Path, None], **kwargs
    ) -> RunSettings:
        """Set up the run."""
        run_settings = copy.deepcopy(self.run_settings)

        if 'run_opt' in kwargs.keys():
            run_settings = replace(run_settings, **kwargs.pop('run_opt'))

        if 'program_opt' in kwargs.keys():
            program_opt = kwargs.pop('program_opt', None)
            if program_opt is None:
                program_opt = {}
        else:
            program_opt = {}

        if data is None:
            return run_settings

        # Create a local work dir
        local_work_dir = Path(run_settings.work_dir_local)
        local_work_dir.mkdir(parents=True, exist_ok=True)

        nopt = sum(
            map(
                bool,
                [
                    kwargs.get('train'),
                    kwargs.get('freeze'),
                    kwargs.get('compress'),
                    kwargs.get('test'),
                    kwargs.get('infer'),
                    # kwargs.get('infer_external'),
                    kwargs.get('model_devi'),
                ],
            )
        )
        if nopt != 1:
            raise ValueError(
                'only one of train, freeze, compress, test, '
                + 'infer model_devi has to be be True'
            )

        for k in [
            'train',
            'freeze',
            'compress',
            'test',
            'infer',
            'model_devi',
            'infer_external',
        ]:
            if kwargs.pop(k, False):
                return getattr(self, f'setup_{k}')(
                    run_settings, data, **program_opt, **kwargs
                )

        raise ValueError('No valid option for setup')

    def setup_test(self, run_settings, data, **kwargs) -> RunSettings:
        """Set up the testing of a model."""
        # Todo: add functionality where force_recompute works.
        # Currently, we delete, if not the files_to_parse will not be parsed
        # from previous calculation, and we get an empty result dict.
        # Not that this is only a problem for the local runner.
        running_dict = {}
        if isinstance(data, DeepMDData):
            data_dir = data.data_dir
        elif isinstance(data, (str, Path)):
            data_dir = data
        else:
            raise ValueError('data must be a DeepMDData or a path')

        if kwargs.get('model') is None:
            if isinstance(data, DeepMDData):
                model = data.model_compressed
            else:
                raise ValueError('model not specified')
        else:
            model = kwargs.get('model')

        if model is None:
            raise ValueError('model not specified')

        model_file = File(name=Path(model), handler=run_settings.file_handler)

        if self.arch_type != 'local':
            graph_filename = model_file.data_path_remote
        else:
            graph_filename = model_file.data_path_local

        hash_str = unique_filename(
            ['test', str(graph_filename), str(data_dir)]
        )
        args = [
            'test',
            '-m',
            str(graph_filename),
            '-s',
            str(data_dir),
            '-d',
            hash_str,
        ]
        data_dir_file = File(
            name=Path(data_dir), handler=run_settings.file_handler
        )
        data_files = [model_file, data_dir_file]
        suffixes = ['.e.out', '.f.out', '.v.out']
        files_to_parse = [
            File(name=hash_str + suffix, handler=run_settings.file_handler)
            for suffix in suffixes
        ]
        running_dict.update(
            {
                'args': args,
                'output_file': hash_str + '.f.out',
                'files_to_parse': files_to_parse,
                'data_files': data_files,
            }
        )

        return replace(run_settings, **running_dict)

    def setup_compress(
        self, run_settings: RunSettings, data, **kwargs
    ) -> RunSettings:
        """Set up the compression of a model."""
        running_dict = {}
        run_settings = copy.deepcopy(run_settings)
        if kwargs.get('check_descriptor', False):
            output = json.load(open(data.output_file, 'rt'))
            if output['model']['descriptor']['type'] not in [
                'se_e2_a',
                'se_e3',
                'se_e2_r',
                'se_atten',
            ]:
                warnings.warn(
                    'model is not a se_e2_a, se_e3, se_e2_r or se_atten model'
                )
                return run_settings
        input_file = File(
            name=Path(data.model), handler=run_settings.file_handler
        )
        data_files = [input_file]

        if self.arch_type != 'local':
            input_filename = input_file.work_path_remote
        else:
            input_filename = input_file.work_path_local

        output_filename = f'{Path(data.model).stem}-compressed.pb'

        args = [
            'compress',
            '-i',
            str(input_filename),
            '-o',
            str(output_filename),
        ]

        running_dict.update(
            {
                'args': args,
                'output_file': output_filename,
                'data_files': data_files,
            }
        )

        if run_settings.launcher == 'srun':
            # then modify lancher in run_settings to use srun -n 1,
            # because this command is not parallelized
            run_settings = replace(
                run_settings, launcher='srun -n1', **running_dict
            )

        run_settings = replace(run_settings, **running_dict)
        if self.debug:
            print('RUN SETTINGS COMPRESS:', run_settings)
        return run_settings

    def setup_freeze(
        self, run_settings: RunSettings, data, **kwargs
    ) -> RunSettings:
        """Set up the freezing of a model."""
        running_dict = {}

        output_file = data.output_file
        if output_file is None:
            raise ValueError('output_file not specified')
        output_filename = f'graph-{Path(output_file).stem}.pb'

        args = ['freeze', '-o', output_filename]

        # Add the model and checkpoint files for freezing
        data_files = []
        local_work_dir = Path(run_settings.work_dir_local)
        # Loop through files inside the work dir, dont do recursive
        for file in local_work_dir.iterdir():
            if 'model' in Path(file).name or 'checkpoint' in Path(file).name:
                data_files.append(
                    File(
                        name=Path(file).name, handler=run_settings.file_handler
                    )
                )

        running_dict.update(
            {
                'args': args,
                'output_file': output_filename,
                'data_files': data_files,
            }
        )

        run_settings = replace(run_settings, **running_dict)
        if self.debug:
            print('RUN SETTINGS FREEZE:', run_settings)

        return run_settings

    def setup_train(
        self,
        run_settings: RunSettings,
        data: Union[list, DataSet, DeepMDData, str, Path],
        **kwargs,
    ) -> RunSettings:
        """Set up the training of a model."""
        running_dict: Dict[str, Any] = {}

        if not isinstance(data, list):
            data = [data]

        train_args = kwargs.pop('train_args', [])
        input_overrides = numb_steps = kwargs.pop('input_override', None)

        load = kwargs.pop('load', True)
        seed = kwargs.pop('seed', 1)
        rand_int_gen = np.random.default_rng(seed=seed)

        save_freq = kwargs.pop('save_freq', 100)
        numb_steps = kwargs.pop('numb_steps', None)

        val_data = kwargs.pop('validation_set', None)

        dp_data = []
        for d in data:
            dp_data.append(self.get_data(d, load=load, **kwargs))

            if dp_data[-1].data_dir is None:
                warnings.warn('deepmd data is not found on disk!')
        val_dp_data = []
        if val_data is not None:
            for d in val_data:
                val_dp_data.append(self.get_data(d, load=load, **kwargs))
                if val_dp_data[-1].data_dir is None:
                    warnings.warn(
                        'deepmd validation data is not found on disk!'
                    )
        paths_training_set: List[str] = []
        data_set_files = []
        for d in dp_data:
            if d.data_dir is None:
                continue
            data_set_files.append(
                File(name=d.data_dir, handler=run_settings.file_handler)
            )
        for f in data_set_files:
            if self.arch_type != 'local':
                paths_training_set.append(str(f.data_path_remote))
            else:
                paths_training_set.append(str(f.data_path_local))
        if len(paths_training_set) == 0:
            raise ValueError('no data to train!')

        paths_validation_set: List[str] = []
        data_set_files_val = []
        for d in val_dp_data:
            if d.data_dir is None:
                continue
            data_set_files_val.append(
                File(name=d.data_dir, handler=run_settings.file_handler)
            )
        for f in data_set_files_val:
            if self.arch_type != 'local':
                paths_validation_set.append(str(f.data_path_remote))
            else:
                paths_validation_set.append(str(f.data_path_local))
        data_set_files += data_set_files_val

        # formulas = [d.type_raw for d in dp_data if d.type_raw is not None]
        formats = [d.format for d in dp_data if d.format is not None]
        if len(set(formats)) > 1:
            raise ValueError('data contains different formats')

        files_to_write = []

        input_dict = kwargs.get('input', self.input)

        input_dict['training']['training_data']['systems'] = paths_training_set
        if len(paths_validation_set) > 0:
            if 'validation_data' not in input_dict['training'].keys():
                input_dict['training']['validation_data'] = {'numb_btch': 1}
            input_dict['training']['validation_data'][
                'systems'
            ] = paths_validation_set
        else:
            if 'validation_data' in input_dict['training'].keys():
                del input_dict['training']['validation_data']
        input_dict['training']['seed'] = int(rand_int_gen.integers(1, 1000000))
        input_dict['model']['fitting_net']['seed'] = int(
            rand_int_gen.integers(1, 1000000)
        )
        if 'list' in input_dict['model']['descriptor'].keys():
            for i in range(len(input_dict['model']['descriptor']['list'])):
                input_dict['model']['descriptor']['list'][i]['seed'] = int(
                    rand_int_gen.integers(1, 1000000)
                )
        else:
            input_dict['model']['descriptor']['seed'] = int(
                rand_int_gen.integers(1, 1000000)
            )
        type_map = []
        for d in dp_data:
            for t in d.type_map_raw:
                if t not in type_map:
                    type_map.append(t)
        input_dict['model']['type_map'] = sorted(type_map)
        input_dict['training']['save_ckpt'] = 'model.ckpt'
        input_dict['training']['save_freq'] = save_freq

        if numb_steps is not None:
            input_dict['training']['numb_steps'] = numb_steps
        # if len(formulas) > 1:
        if data[0].format == 'mixed_type':
            input_dict['model']['descriptor']['type'] = 'se_atten'

        # Insert overrides from user through the "input_override" keyword

        def update_nested_dict(d, u):
            for k, v in u.items():
                if isinstance(v, dict):
                    d[k] = update_nested_dict(d.get(k, {}), v)
                else:
                    d[k] = v
            return d

        if input_overrides is not None:
            update_nested_dict(input_dict, input_overrides)

        input_str = json.dumps(input_dict, indent=4)
        sorted_input_str = json.dumps(
            input_dict, indent=4, sort_keys=True, separators=(',', ': ')
        )
        hash_str = unique_filename(sorted_input_str.split('\n'))
        input_filename = hash_str + '.json'

        files_to_write.append(File(name=input_filename, content=input_str))
        output_filename = hash_str + '.out'

        args = [
            'train',
            '-o',
            # str(output_file_local),  # <- container (?) or local
            str(output_filename),
            str(input_filename),
        ]

        output_file = output_filename
        running_dict.update(
            {
                'files_to_write': files_to_write,
                'args': args + train_args,
                'output_file': output_file,
                'data_files': data_set_files,
            }
        )

        run_settings = replace(run_settings, **running_dict)
        if self.debug:
            print('RUN SETTINGS TRAIN:', run_settings)
        return run_settings

    #  Note: run_sequentially is not used
    def parse(self, output: ComputeResult, **kwargs) -> dict:
        """Parse output."""
        result: Dict[str, Union[str, list, dict, float, np.ndarray]] = {}
        if self.debug:
            print('PARSER, output:', output)
        if output.files_to_parse:
            if not isinstance(output.files_to_parse, list):
                output.files_to_parse = [output.files_to_parse]
            for f in output.files_to_parse:
                if isinstance(f, File):
                    p = Path(f.work_path_local)
                elif isinstance(f, str):
                    p = Path(f)
                elif not isinstance(f, Path):
                    raise TypeError(
                        'files_to_parse must be a list of File, str or Path'
                    )
                if not p.exists():
                    result['is_converged'] = [False]
                    return result

                p_name = p.name
                found = False
                for key in ['.e.out', '.f.out', '.v.out']:
                    if p_name.endswith(key):
                        replacements = (
                            ('.e.out', 'energy'),
                            ('.f.out', 'force'),
                            ('.v.out', 'virial'),
                        )
                        dict_key = key
                        for r in replacements:
                            dict_key = dict_key.replace(*r)
                        result[dict_key] = np.loadtxt(str(p))
                        found = True
                        continue
                if found:
                    continue

                if p.suffix == '.out':
                    with open(p, 'rt') as ff:
                        result[str(p)] = ff.read()
                elif p.suffix == '.json':
                    with open(p, 'rt') as ff:
                        result.update(json.load(ff))

                elif p.suffix == '.pb':
                    result['model'] = str(p)
        if output.output_file:
            output_file = Path(output.output_file)

            if not output_file.exists():
                result['is_converged'] = [False]
                return result

            if output_file.suffix == '.pb':
                if 'compress' in output_file.name:
                    result['model_compressed'] = str(output_file)
                else:
                    result['model'] = str(output_file)
            else:
                with open(output_file, 'rt') as f:
                    result['output'] = {}
                    try:
                        result.update(json.load(f))
                    except json.decoder.JSONDecodeError:
                        try:
                            df = pd.read_csv(
                                output_file,
                                skiprows=1,
                                header=0,
                                delim_whitespace=True,
                            )
                        except Exception:
                            raise Exception('could not read output file')
                        else:
                            dd = df.to_dict()
                            ll = list(dd.keys())
                            lm = ll[1:] + ['nan']
                            d = {lm[i]: dd[ll[i]] for i in range(len(lm) - 1)}
                            result['output'].update(d)  # type: ignore

            result['output_file'] = str(output_file)

        if output.stdout:
            if 'DeePMD' in output.stdout:
                result['version'] = output.stdout.split()[1].replace('v', '')
            result['stdout'] = output.stdout

        if output.stderr:
            result['stderr'] = output.stderr

            for line in output.stderr.split('\n'):
                if 'Energy RMSE ' in line:
                    result['energy_rmse'] = float(line.split()[5])
                if 'Energy RMSE/Natoms' in line:
                    result['energy_rmse_per_atom'] = float(line.split()[5])
                if 'Force  RMSE' in line:
                    result['force_rmse'] = float(line.split()[5])
                if 'Virial RMSE ' in line:
                    result['virial_rmse'] = float(line.split()[5])
                if 'Virial RMSE/Natoms' in line:
                    result['virial_rmse_per_atom'] = float(line.split()[5])

        return result

    def author(self):
        """Get author of this interface."""
        return 'tilmannb@uio.no'

    def run(self, data, *args, **kwargs):
        """Run dp calculation context-based."""
        with compute(self, data, *args, **kwargs) as r:
            return r

    async def arun(self, data, *args, **kwargs):
        """Run dp calculation context-based and asyncronous."""
        async with acompute(self, data, *args, **kwargs) as r:
            return r

    def preload_model(
        self, model: str, run_settings: Optional[RunSettings] = None
    ) -> Any:
        """Preload model."""
        if run_settings is None:
            run_settings = self.run_settings
        if not is_file(model):
            model_file = File(
                name=Path(model), handler=run_settings.file_handler
            )
            model2 = model_file.data_path_local
            if is_file(model2):
                model = model2
            else:
                raise FileNotFoundError(f'could not find model {model}')
        try:
            from deepmd.infer import DeepPot
        except ImportError:
            raise ImportError('could not find deepmd-kit')

        return DeepPot(str(model))

    def setup_infer_external(self, model, data, **kwargs) -> RunSettings:
        """Set up the inference of a model."""
        run_settings = copy.deepcopy(self.run_settings)
        if 'program_opt' in kwargs.keys():
            program_opt = kwargs.pop('program_opt', None)
            if program_opt is None:
                program_opt = {}
        else:
            program_opt = {}

        # check for run_opt
        if 'run_opt' in kwargs.keys():
            run_settings = replace(run_settings, **kwargs.pop('run_opt'))
            # make sure that work_dir_local is existing
            run_settings.work_dir_local.mkdir(parents=True, exist_ok=True)

        if isinstance(data, DataSet):
            dataset = data
        else:
            raise ValueError(
                'data must be a DeepMDData, a path, or a DataSet.'
            )

        # check if multiple models are given
        if isinstance(model, List):
            models = copy.deepcopy(model)
        else:
            models = [model]

        dataset_tmp = copy.deepcopy(dataset)

        if dataset_tmp.has_units:
            dataset_tmp.change_units(self.units)
        else:
            if self.debug:
                warnings.warn(
                    'dataset has no units, using default units for inference'
                )
            dataset_tmp.change_units(self.units)
        dataset_tmp.standardize_columns()
        hash_dataset = dataset_tmp.hash

        model_hashes = []
        for model in models:
            if isinstance(model, (str, Path)):
                model_basename = Path(model).name
                if model_basename.endswith('.pb'):
                    model_hashes.append(model_basename)
                else:
                    model_hashes.append(unique_filename([str(model)]))
            else:
                raise ValueError(
                    'model must be a path for external inference.'
                )

        hash_out = unique_filename(
            [hash_dataset]
            + model_hashes
            + [str(kwargs.get('full_output', False))]
        )
        # if kwargs.get('cleanup', False):
        # use same hash for input and output
        hash_in = hash_out
        input_file = File(
            name=hash_in + '_in.pkl', handler=run_settings.file_handler
        )
        dataset_tmp.write_data(input_file.work_path_local)
        model_files = []
        model_filenames = []
        for model in models:
            model_file = File(
                name=Path(model), handler=run_settings.file_handler
            )
            model_files.append(model_file)
            if self.arch_type != 'local':
                model_filenames.append(model_file.data_path_remote.name)
            else:
                model_filenames.append(model_file.data_path_local)

        if self.arch_type != 'local':
            data_filename = input_file.data_path_remote.name
            # Make sure is a basename
            data_filename = Path(data_filename).name
        else:
            data_filename = input_file.work_path_local

        data_files = [
            File(
                name=input_file.work_path_local,
                handler=run_settings.file_handler,
            )
        ] + model_files
        output_file = File(name=f'{hash_out}_out.pkl')

        model_filenames = [f"\"{m}\"" for m in model_filenames]
        model_hashes = [f"\"{m}\"" for m in model_hashes]
        data_filename = str(data_filename)
        output_filename = str(output_file.name)
        joined_model_filenames = ', '.join(model_filenames)
        joined_model_hashes = ', '.join(model_hashes)
        dataset_id = f"\"{hash_dataset}\""
        input_str = """
import pandas as pd
import os
import numpy as np
from deepmd.infer import DeepPot

data_in = pd.read_pickle('{data_filename}')
if 'dataset_id' in data_in.columns:
    dataset_ids = data_in['dataset_id'].values
else:
    data_in['dataset_id'] = [{dataset_id}]*len(data_in)
    data_in['dataset_id'].astype('category')
    dataset_ids = data_in['dataset_id'].values[0]

if 'geometry_id' not in data_in.columns:
    data_in['geometry_id'] = np.arange(len(data_in))
    data_in['geometry_id'].astype('category')

graphs = [DeepPot(model) for model in [{model_filenames}]]

{infer_minimal_function}

df = infer_minimal(graphs, model_hashes=[{model_hashes}],
                   df=data_in, full_output={full_output})

df['energy'] = df['energy'].astype(np.float64)
df['forces'] = df['forces'].apply(lambda x: np.array(x, dtype=np.float64))
df['virial'] = df['virial'].apply(lambda x: np.array(x, dtype=np.float64))

if {full_output}:
    if 'box' in df.columns:
        df['box'] = df['box'].apply(lambda x: np.array(x, dtype=np.float64))

    df['coordinates'] = df['coordinates'].apply(lambda x:
                                                np.array(x, dtype=np.float64))
    df['atoms'] = df['atoms'].apply(lambda x: np.array(x))
    df['method_id'] = df['method_id'].astype('category')
    df['dataset_id'] = df['dataset_id'].astype('category')
    df['geometry_id'] = df['geometry_id'].astype(np.int64)


pd.to_pickle(df, '{output_filename}')
""".format(
            model_filenames=joined_model_filenames,
            model_hashes=joined_model_hashes,
            data_filename=data_filename,
            output_filename=output_filename,
            dataset_id=str(dataset_id),
            full_output=str(kwargs.get('full_output', False)),
            infer_minimal_function=inspect.getsource(infer_minimal),
        )

        # hash_in = unique_filename(input_str.split('\n'))
        input_filename = hash_out + '.py'
        input_script = File(name=input_filename, content=input_str)
        files_to_write = [input_script]
        files_to_parse = [input_script] + [output_file] + [input_file]

        running_dict = {
            'program': 'python',
            'args': str(input_script.name),
            'files_to_parse': files_to_parse,
            'files_to_write': files_to_write,
            'output_file': output_file,
            'data_files': data_files,
        }
        run_settings = replace(run_settings, **running_dict)
        return run_settings

    def parse_infer_external(self, output: ComputeResult, **kwargs) -> dict:
        """Parse output."""
        out_dict = {}
        try:
            out_dict.update(
                DataSet(
                    Path(output.output_file), units=self.units
                ).data.to_dict(orient='list')
            )

            if kwargs.get('cleanup', True):
                for filename in output.files_to_parse:
                    filename.unlink()
        except Exception:
            out_dict.update(dict(is_converged=[False]))

        return out_dict

    def infer_external(self, model, data, **kwargs):
        """Infer model on Dataset object.

        Arguments
        ---------
        model (str): The model to infer with.
        data (DataSet): The dataset to infer on, for hetrogenous data,
        use keyword "dataset_id"
        to infer on chunks with the atoms.
        **kwargs: Additional keyword arguments.

        Returns
        -------
        dict: The output of the inference.

        """
        setup_tmp = self.setup_infer_external(model, data, **kwargs)
        unparsed_output = self.compute_settings.run(setup_tmp)
        return self.parse_infer_external(unparsed_output, **kwargs)

    async def ainfer_external(self, model, data, **kwargs):
        """Infer model on Dataset object asynchronously.

        Arguments
        ---------
        model (str): The model to infer with. Multiple models can be
        given as a list of strings.
        data (DataSet): The dataset to infer on, for hetrogenous data,
        use keyword "dataset_id" and "geometry_id".
        to infer on chunks with the atoms.
        **kwargs: Additional keyword arguments.

        Returns
        -------
        dict: The output of the inference.

        """
        setup_tmp = self.setup_infer_external(model, data, **kwargs)
        unparsed_output = await self.compute_settings.arun(setup_tmp)
        return self.parse_infer_external(unparsed_output, **kwargs)

    # Duplicate ainfer_external to ainfer
    ainfer = ainfer_external

    def infer(
        self,
        model: Optional[data_type] = None,
        data: Optional[data_type] = None,
        **kwargs,
    ):
        """Infer result from model."""
        if kwargs.get('run_external', False):
            return self.infer_external(model, data, **kwargs)
        if model is None:
            raise ValueError('model not specified')
        run_settings = kwargs.pop('run_settings', None)
        if run_settings is None:
            run_settings = self.run_settings
        if not isinstance(model, List):
            models = [model]

        # Loop over models
        graphs = []
        model_hashes = []
        for model_idx, model_it in enumerate(models):
            if isinstance(model_it, (str, Path)):
                model_hash = Path(model_it).name
                try:
                    graph = self.preload_model(
                        str(model_it), run_settings=run_settings
                    )
                except ImportError:
                    return self.infer_external(
                        model, data, run_settings=run_settings
                    )
            else:  # use preloaded model
                graph = model_it
                try:
                    model_hash = kwargs.get('model_hashes')[model_idx]
                except Exception:
                    model_hash = f'model_{model_idx}'

            model_hashes.append(model_hash)
            graphs.append(graph)

        if isinstance(data, DataSet):
            ds = copy.deepcopy(data)
            if ds.has_units:
                ds.change_units(self.units)
            else:
                if self.debug:
                    warnings.warn(
                        'dataset has no units, '
                        + 'using default units for inference'
                    )
                ds = DataSet(ds.data, units=self.units)
            ds.standardize_columns()

            if 'dataset_id' not in ds.data.columns:
                ds.data['dataset_id'] = ['dummy'] * len(ds.data)
            ds.data['dataset_id'] = ds.data['dataset_id'].astype('category')
            if 'geometry_id' not in ds.data.columns:
                ds.data['geometry_id'] = np.arange(len(ds.data))
                ds.data['geometry_id'].astype('category')
        else:
            raise ValueError('data must be a DataSet')

        try:
            return infer_minimal(
                graphs=graphs, model_hashes=model_hashes, df=ds.data, **kwargs
            ).to_dict(orient='list')
        except Exception as e:
            if self.debug:
                print(e)
            return {'is_converged': [False] * len(ds.data)}

    def model_deviation(
        self, data: data_type, models: Union[str, Path, list], **kwargs
    ):
        """Get model deviation."""
        raise NotImplementedError(
            'Removed, use model_deviation as implemented in hyal instead.'
        )

    def get_type_map_from_dir(self, data: Union[str, Path]):
        """Get type map."""
        for p in Path(data).rglob('type_map.raw'):
            with open(p, 'rt') as f:
                type_map_raw = f.read().split()
                break
        else:
            raise FileNotFoundError('could not find type_map.raw file')

        return type_map_raw

    def get_type_map_from_model(self, model: Any):
        """Get type map."""
        if isinstance(model, (str, Path)):
            graph = self.preload_model(str(model))
        else:
            graph = model
        return graph.get_type_map()

    def get_type_map_from_input(self, input: Union[str, Path, dict]):
        """Get type map."""
        if isinstance(input, (str, Path)):
            input_dict = json.load(open(input, 'rt'))
        else:
            input_dict = input

        def recursive_search(dictionary, target):
            for k, v in dictionary.items():
                if k == target:
                    return v
                elif isinstance(v, dict):
                    found = recursive_search(v, target)
                    if found is not None:
                        return found
                elif isinstance(v, list):
                    for d in v:
                        if isinstance(d, dict):
                            found = recursive_search(d, target)
                            if found is not None:
                                return found
            return None

        type_map = recursive_search(input_dict, 'type_map')
        if type_map is None:
            raise ValueError('type_map not found in input')
        return type_map

    def get_type_map(self, any_arg: Any = None):
        """Get type map for DeePMD model."""
        warning_msg = (
            'Using type_map from constructor or previous training.'
            ' This can be unreliable.'
        )
        if isinstance(any_arg, Dict):
            try:
                return self.get_type_map_from_input(any_arg)
            except ValueError:
                pass

        if any_arg is None:
            warnings.warn(warning_msg)
            type_map = self.type_map
            if type_map is None:
                raise ValueError('Type map cannot be inferred.')
            else:
                return type_map

        try:
            return self.get_type_map_from_model(any_arg)
        except Exception:
            pass
        if isinstance(any_arg, (str, Path)):
            try:
                return self.get_type_map_from_dir(any_arg)
            except Exception:
                pass
            try:
                return self.get_type_map_from_input(any_arg)
            except Exception:
                pass
        warnings.warn(warning_msg)
        type_map = self.type_map
        if type_map is None:
            raise ValueError('Type map cannot be inferred.')
        else:
            return type_map


def infer_minimal(graphs, model_hashes, df, **kwargs):
    """Perform inference using DeepMD models.

    Arguments
    ---------
        graphs (list): List of DeepMDGraph objects.
        model_hashes (list): List of model hashes.
        df (pandas.DataFrame): DataFrame containing the dataset.
        **kwargs: Additional keyword arguments.

    Returns
    -------
        pandas.DataFrame: DataFrame containing the inference results.

    """
    results = pd.DataFrame()
    pbc = 'box' in df.columns

    # Loop over dataset_ids
    for dataset_id in np.unique(df['dataset_id']):
        data = df[df['dataset_id'] == dataset_id]
        if pbc:
            box = np.vstack(data['box'])
        else:
            box = None
        coord = np.vstack(data['coordinates'])
        atoms = data['atoms'].values[0]
        natoms = len(atoms)
        for graph, model_hash in zip(graphs, model_hashes):
            result = {}

            # Set atom types
            type_map = graph.get_type_map()
            atypes = [type_map.index(atom) for atom in atoms]

            e, f, v = graph.eval(coord, box, atypes)
            nframes = len(e)

            nframes = len(e)
            result = {
                'energy': e.flatten().tolist(),
                'forces': np.array(f).reshape(nframes, natoms * 3).tolist(),
                'virial': np.array(v).reshape(nframes, 9).tolist(),
            }
            if kwargs.get('full_output', False):
                result.update(
                    {
                        'coordinates': coord.tolist(),
                        'atoms': [atoms] * nframes,
                        'method_id': [model_hash] * nframes,
                        'dataset_id': [dataset_id] * nframes,
                        'geometry_id': np.array(data['geometry_id'].values)
                        .flatten()
                        .tolist(),
                    }
                )
                if pbc:
                    result['box'] = box.tolist()

            if len(results) == 0:
                results = pd.DataFrame(result)
            else:
                results = pd.concat([results, pd.DataFrame(result)])
    return results
