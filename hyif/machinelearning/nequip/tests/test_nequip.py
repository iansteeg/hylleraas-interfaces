import copy
import pathlib

import ase.io
import numpy as np
from hyobj import DataSet
from hyset import create_compute_settings

from hyif import VASP
from hyif.machinelearning.nequip import NequIP


def test_setup_stress_virial_from_vasp(request):
    """Test NequIP."""
    cs = create_compute_settings('local', sub_dir='test_train')

    vasp = VASP({})
    parser = vasp.OutputParser

    ds = DataSet(
        pathlib.Path(request.path).parent
        / 'vasp_test_outputs'
        / 'frame_0'
        / 'vasprun.xml',
        parser=parser,
        units='metal',
    ) + DataSet(
        pathlib.Path(request.path).parent
        / 'vasp_test_outputs'
        / 'frame_1'
        / 'vasprun.xml',
        parser=parser,
        units='metal',
    )

    mynequip = NequIP(
        {
            'input': pathlib.Path(request.path).parent / 'test.yaml',
            'check_version': False,
        },
        compute_settings=cs,
    )

    mynequip.train(
        ds,
        validation_set=copy.deepcopy(ds),
        max_epochs=10,
        device='cpu',
        program_opt={'seed': 123},
        debug=True,
    )

    vasprun_xml_stress = (
        -1
        / 1602.1766208
        * np.array(
            [
                [-12.45934867, 1.04189259, -3.69724965],
                [1.04188050, -73.10860248, 4.52749651],
                [-3.69724575, 4.52750592, -86.83211389],
            ]
        ).reshape(-1)
    )

    np.testing.assert_almost_equal(
        ds.data.iloc[0].stress, vasprun_xml_stress, decimal=6
    )
    with open('test_train/test.xyz', 'w') as ofile:
        ofile.write(ds[0].to_extxyz_string())
    stresses_ase = (
        ase.io.read('test_train/test.xyz').get_stress(voigt=False).reshape(-1)
    )
    np.testing.assert_almost_equal(stresses_ase, vasprun_xml_stress, decimal=6)


def test_integration(request):
    """Test NequIP."""
    cs = create_compute_settings('local', sub_dir='test_train')

    mynequip = NequIP(
        {
            'input': pathlib.Path(request.path).parent / 'test.yaml',
            'check_version': False,
        },
        compute_settings=cs,
    )

    v = mynequip.check_version()
    assert v is not None

    ds = DataSet(
        str(pathlib.Path(request.path).parent / 'test.xyz'), units='metal'
    )

    # train and deploy
    out_train = mynequip.train(
        ds,
        validation_fraction=0.2,
        max_epochs=10,
        device='cpu',
        program_opt={'seed': 123},
        debug=True,
    )
    assert pathlib.Path(out_train['model']).exists()

    # # test in the config not used in the train and validation
    # out_test = mynequip.test(
    #     out_train['train']['out_dir'],
    #     model=out_train['model'],
    #     device='cpu',
    #     debug=True,
    # )

    # assert [
    #     k in ['stderr', 'stdout', 'e_rmse', 'e/N_rmse', 'f_rmse']
    #     for k in out_test.keys()
    # ]

    # assert pytest.approx(out_test['f_rmse']) == 0.112305
    # assert pytest.approx(out_test['e_rmse']) == 0.003556
    # assert pytest.approx(out_test['e/N_rmse']) == 0.001185

    # infer the type mapping from the train
    assert mynequip.get_type_map(out_train) == ['H', 'O']

    # infer the type mapping from the model
    assert mynequip.get_type_map(out_train['model']) == ['H', 'O']

    # test infer using the same configs
    out_infer = mynequip.infer(
        data=pathlib.Path(request.path).parent / 'test.xyz',
        model=out_train['model'],
        device='cpu',
        debug=True,
    )
    np.testing.assert_allclose(
        out_infer['energy'],
        [
            -100.00355562568674,
            -100.00355562568674,
            -100.00355562568674,
            -100.00355562568674,
            -100.00355562568674,
        ],
        rtol=1e-1,
    )
