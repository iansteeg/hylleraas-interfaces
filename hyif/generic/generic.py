import copy
import json
from dataclasses import replace
from functools import singledispatchmethod
from pathlib import Path
from typing import Any, Optional, Union

from hyset import ComputeSettings, File, RunSettings, create_compute_settings

from ..manager import NewRun
from ..utils import is_file, unique_filename

# try:
#     from jinja2 import Template
# except ImportError:
#     Template = None
try:
    from flask import render_template
except ImportError:
    render_template = None


class Generic(NewRun):
    """Generic class for running calculations."""

    def __init__(
        self, method: dict, compute_settings: Optional[ComputeSettings] = None
    ):
        """Initialize."""
        self.method = method
        if compute_settings is None:
            self.compute_settings = create_compute_settings()
        else:
            self.compute_settings = compute_settings

        self.Runner = self.compute_settings.Runner

        self.InputParser = None
        self.OutputParser = None

        self.run_settings = self.compute_settings.run_settings
        self.run_settings.program = method.get('program', 'python')

    @singledispatchmethod
    def setup(self, obj, **kwargs) -> RunSettings:
        """Set up calculation."""
        raise TypeError(f'obj type {type(obj)} not supported')

    @setup.register(Path)
    @setup.register(str)
    def _(self, obj: Union[str, Path], **kwargs):
        """Set up calculation."""
        run_settings = copy.deepcopy(self.run_settings)
        if 'run_opt' in kwargs.keys():
            run_settings = replace(run_settings, **kwargs.pop('run_opt'))

        if obj is None:
            return run_settings

        running_dict = {}

        if is_file(obj):
            with open(obj, 'r') as f:
                inp = f.read()
        else:
            inp = str(obj)

        if all(['{{' in inp, '}}' in inp]):
            # if Template is None:
            #     raise ImportError('jinja2 not installed')
            # template = Template(inp)
            # inp = template.render(**kwargs)
            # safer alternative to jinja2:
            if render_template is None:
                raise ImportError('flask not installed')
            inp = render_template(inp, **kwargs)

        try:
            files_to_write = run_settings.files_to_write
        except AttributeError:
            files_to_write = []

        hash_str = kwargs.pop('filename', None)
        if hash_str is None:
            hash_str = unique_filename(inp.split('\n'))
        input_file_name = hash_str + '.inp'
        input_file = File(name=input_file_name, content=inp)

        output_file_name = kwargs.pop('output_file', False)
        if not output_file_name:
            output_file_name = hash_str + '.out'

        # TODO: This code looks redundant, but is kept for now.
        if output_file_name in inp:
            running_dict.update({'output_file': output_file_name})
        else:
            running_dict.update({'stdout_redirect': output_file_name})

        files_to_write.append(input_file)

        args = run_settings.args if run_settings.args else []
        args.insert(0, f'{input_file_name}')

        running_dict.update({'args': args, 'files_to_write': files_to_write})
        run_settings = replace(run_settings, **running_dict)
        return run_settings

    def parse_output(self, file: Union[str, Path]) -> Union[dict, str]:
        """Parse output file."""
        # try to read as a json file, else do it as a text file
        if not Path(file).exists():
            return dict(is_converged=False, error='File not found')
        try:
            with open(file, 'r') as f:
                return json.load(f)
        except json.decoder.JSONDecodeError:
            with open(file, 'r') as f:
                return f.read()

    def parse(self, output: Any) -> dict:
        """Parse output."""
        result = {}
        if output.output_file:
            result.update({'output': self.parse_output(output.output_file)})
        if output.files_to_parse:
            if not isinstance(output.files_to_parse, list):
                output.files_to_parse = [output.files_to_parse]
            for f in output.files_to_parse:
                if isinstance(f, File):
                    p = Path(f.work_path_local)
                elif isinstance(f, str):
                    p = Path(f)
                else:
                    p = f
                result.update({str(p): self.parse_output(p)})
        if output.stdout:
            result.update({'stdout': output.stdout})
        try:
            self.parse_output('result.out')
        except FileNotFoundError:
            pass
        else:
            result.update({'result': self.parse_output('result.out')})
        return result


if __name__ == '__main__':
    file_input = '''
from hyif import Xtb
from hyobj import Molecule
from hyset import create_compute_settings

myenv = create_compute_settings('local')
mymol = Molecule({{smiles}}, units='angstrom')
mycalc = Xtb({'check_version': False}, compute_settings=myenv)
en = mycalc.get_energy(mymol)
print('energy:', en)
with open('/cluster/work/users/tilmann/data_files/ref_energy', 'r') as f:
    ref_energy = float(f.read().split()[-1])
print('ref_energy:', ref_energy)

with open('result_data.out', 'w') as f:
    f.write(f'energy: {en}\\nref_energy: {ref_energy}')
'''
    data_file = '''
ref_energy: -5.069274837138
    '''

    with open('ref_energy', 'w') as f:
        f.write(data_file)

    env_saga = [
        'source ${EBROOTANACONDA3}/bin/activate',
        'conda activate /cluster/projects/nn4654k/tilmann/conda/conf_ref',
    ]

    cs_saga = create_compute_settings(
        'saga',
        modules=['Anaconda3/2022.10', 'xtb/6.4.1-intel-2021a'],
        ssh_username='tilmann',
        debug=True,
        env=env_saga,
    )

    mygen = Generic({'program': 'python'}, compute_settings=cs_saga)
    out = mygen.run(
        file_input, smiles="'O'", run_opt={'data_files': ['ref_energy']}
    )

    def output_parser(output_file: str) -> dict:
        """Parse output."""
        result = {}
        with open(output_file, 'r') as f:
            lines = f.readlines()
        for line in lines:
            try:
                key, val = line.split(':')
            except ValueError:
                continue
            result.update({key.strip(): float(val.strip())})
        return result

    print(output_parser('result_data.out'))
