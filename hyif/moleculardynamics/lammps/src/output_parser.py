import glob
from pathlib import Path
from typing import Union

try:
    import MDAnalysis as Mda
except ImportError:
    Mda = None
import numpy as np

from ...abc import Parser


class OutputParser(Parser):
    """Lammps OutputParser."""

    @classmethod
    def parse(cls, filename: Union[str, Path]) -> dict:
        """Parse LAMMPS reajectory file."""
        boxlist = []
        coords = []
        atypes = []
        if Mda is None:
            raise ImportError('MDAnalysis is not installed.')

        results = {}
        try:
            u = Mda.Universe(str(filename), format='LAMMPSDUMP')
            for ts in u.trajectory:
                boxlist.append(
                    Mda.lib.mdamath.triclinic_vectors(u.dimensions).ravel()
                )
                coords.append(u.atoms.positions.ravel())
                atypes.append(np.array(u.atoms.types, dtype=int))
            num_frames = len(boxlist)
            traj = {
                'types': atypes,
                'coordinates': np.array(coords).reshape(num_frames, -1),
                'box': np.array(boxlist).reshape(num_frames, -1),
            }
            results.update(traj)

        except Exception as e:
            results['error'] = 'Could not parse LAMMPS trajectory: ' + str(e)
            results['is_converged'] = False

        return results

    @classmethod
    def parse_files(cls, filename: Union[str, Path]) -> dict:
        """Get a list of lammps output files."""
        hash_prefix = str(filename).replace('.lammpstrj', '')

        files = {}
        for fn in glob.glob(hash_prefix + '*'):
            key = fn.replace(hash_prefix + '.', '')
            files[key] = fn

        return {'files': files}

    @classmethod
    def parse_stdout(cls, stdout: Union[str, Path]) -> dict:
        """Parse LAMMPS stdout."""
        output = {}

        content = stdout.read_text() if isinstance(stdout, Path) else stdout

        for line in content.split('\n'):
            if 'LAMMPS' in line:
                output['version'] = line
            if 'OpenMP thread(s)' in line:
                output['OpenMP_threads'] = line.split()[1]
            if 'Total wall time' in line:
                output['wall_time'] = line.split()[3]
            if 'Error' in line:
                output['error'] = line.split('Error:')[1]

        return output

    @classmethod
    def parse_dir(cls, directory: Union[str, Path]) -> dict:
        """Parse LAMMPS directory."""
